# CIT_for_Computation

What is computation if not the structure of the invariant set of the system that implements the computation?

How else to capture this structure than through topology?
